import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux'
import thunk from 'redux-thunk'

import postReducer from './reducers/postReducer'

import './index.css';
import App from './App';

const store = createStore(
  postReducer,
  applyMiddleware(thunk)
);

const Root = () => (
  <Provider store={store}>
    <App />
  </Provider>
)

ReactDOM.render(<Root />, document.getElementById('root'));
