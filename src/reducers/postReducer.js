import {
  ADD_POST,
  EDIT_POST,
  DELETE_POST,
  LOAD_POST,
  UPDATE,
} from './../types/postType'

const postReducer = (state = [], action) => {
  switch (action.type) {
    case LOAD_POST:
      return state = action.data
      break;
    case ADD_POST:
      return state.concat([action.data]);
      break;
    case DELETE_POST:
      const id = action.id;
      return state.filter((post) => post.id !== id);
      break;
    case EDIT_POST:
      return state
        .map((post)=> post.id === action.id
          ? {
              ...post,
              editing: !post.editing
            }
          : post
        );
      break;
    case UPDATE:
      return state.map((post)=> {
        if (post.id === action.id) {
          return {
            ...post,
            title: action.data.newTitle,
            body: action.data.newMessage,
            editing: !post.editing
          }
        }

        return post
      });
      break;
    default:
      return state;
      break;
  }
};

export default postReducer;
