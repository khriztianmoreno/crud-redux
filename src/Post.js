import React, { PureComponent } from 'react';
import {connect} from 'react-redux';

class Post extends PureComponent {
  constructor () {
    super();

    this.state = {
      title: '',
      body: ''
    }
  }

  render() {
    const { dispatch, post } = this.props
    return (
      <div className="post">
        <h2 className="post_title">{post.title}</h2>
        <p className="post_message">{post.body}</p>
        <button
          className="edit"
          onClick={()=> dispatch({type:'EDIT_POST',id:post.id})}
        >
          Edit
        </button>
        <button
          className="delete"
          onClick={()=> dispatch({type:'DELETE_POST',id:post.id})}
        >
        Delete
        </button>
      </div>
    );
 }
}

export default connect()(Post);
