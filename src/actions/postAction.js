import axios from 'axios';

import {
  ADD_POST,
  LOAD_POST,
} from './../types/postType'

export function loadPost() {
  return dispatch => {
    axios.get('https://jsonplaceholder.typicode.com/posts/')
      .then(({ data }) => {
        dispatch({ type: LOAD_POST, data });
      })
      .catch(err => console.log(err));
  };
}

export function createPost(data) {
  return {
    type: ADD_POST,
    data,
  };
}
