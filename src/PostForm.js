import React, { Component } from 'react'
import { connect } from 'react-redux';

import { createPost } from './actions/postAction'

class PostForm extends Component {
  constructor () {
    super();

    this.state = {
      title: '',
      body: ''
    }

    // this.handleSubmit = this.handleSubmit.bind(this)
  }

  handleSubmit (e){
    e.preventDefault();
    const { title, body } = this.state;

    const data = {
      id: new Date(),
      title,
      body,
      editing: false
    };

    /* this.props.dispatch(
      {
        type:'ADD_POST',
        data
      }
    ); */

    this.props.dispatch(createPost(data))

    this.setState({
      title: '',
      body: ''
    })
  }

  render () {
    return (
      <div className="post-container">
        <h1 className="post_heading">Create Post</h1>
        <form
          className="form"
          onSubmit={(e) => { this.handleSubmit(e); }}
        >
          <input
            required
            type="text"
            onChange={(e) => {
              this.setState({ title: e.target.value })
            }}
            value={this.state.title}
            placeholder="Enter Post Title"
          />
          <br /><br />
          <textarea
            required
            onChange={(e) => {
              this.setState({ body: e.target.value })
            }}
            rows="5"
            cols="28"
            placeholder="Enter Post"
          />
          <br /><br />
          <button>Post</button>
        </form>
      </div>
    )
  }
}

export default connect()(PostForm);
