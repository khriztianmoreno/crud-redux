import React, { Component } from 'react';
import { connect } from 'react-redux';

import { loadPost } from './actions/postAction'

import Post from './Post';
import EditComponent from './EditComponent'

class AllPost extends Component {
  componentDidMount () {
    this.props.loadPost()
  }

  render() {
    return (
    <div>
      <h1 className="post_heading">All Posts</h1>
      {
        this.props.posts &&
        this.props.posts.length > 0 &&
        this.props.posts.map((post, i) => (
          <div key={post.id}>
            {
              post.editing
                ? <EditComponent post={post} key={post.id} />
                : <Post key={post.id} post={post} />
            }
          </div>
        ))
      }
    </div>
    );
   }
}

const mapStateToProps = (state) => {
  return {
    posts: state
  }
}

const mapDispatchToProps = {
  loadPost
}

export default connect(mapStateToProps, mapDispatchToProps)(AllPost);
