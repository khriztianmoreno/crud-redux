import React, { Component } from 'react';
import { connect } from 'react-redux';

class EditComponent extends Component {
  constructor () {
    super();

    this.state = {
      newTitle: '',
      newMessage: ''
    }

    this.handleEdit = this.handleEdit.bind(this)
  }

  handleEdit (e) {
    e.preventDefault();
    const { newTitle, newMessage } = this.state
    const { title, message } = this.props.post

    const data = {
      newTitle: newTitle || title,
      newMessage: newMessage || message
    }

    this.props.dispatch(
      {
        type: 'UPDATE',
        id: this.props.post.id,
        data
      }
    )
  }

  render () {
    return (
      <div key={this.props.post.id} className="post">
        <form className="form" onSubmit={this.handleEdit}>
          <input
            required
            type="text"
            onChange={(e) => {
              this.setState({ newTitle: e.target.value })
            }}
            defaultValue={this.props.post.title}
            placeholder="Enter Post Title"
          /><br /><br />
          <textarea
            required
            rows="5"
            onChange={(e) => {
              this.setState({ newMessage: e.target.value })
            }}
            defaultValue={this.props.post.body}
            cols="28"
            placeholder="Enter Post"
          /><br /><br />
          <button>Update</button>
        </form>
      </div>
    );
  }
}

export default connect()(EditComponent);
